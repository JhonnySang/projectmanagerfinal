USE [AdministradorDeProyectos1]
GO

/****** Object:  Table [dbo].[Solicitudes]    Script Date: 10/9/2019 10:42:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Solicitudes](
	[IdSolicitud] [int] IDENTITY(1,1) NOT NULL,
	[Titulo] [varchar](128) NOT NULL,
	[Descripcion] [varchar](256) NOT NULL,
	[Estado] [int] NOT NULL,
	[IdUnidadDeNegocio] [int] NULL,
 CONSTRAINT [PK_Solicitudes] PRIMARY KEY CLUSTERED 
(
	[IdSolicitud] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


