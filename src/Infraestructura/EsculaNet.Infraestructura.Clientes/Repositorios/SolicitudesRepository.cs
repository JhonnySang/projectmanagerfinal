﻿using EscuelaNet.Dominio.Clientes;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsculaNet.Infraestructura.Clientes.Repositorios
{
    public class SolicitudesRepository : ISolicitudRepository
    {
        private ClienteContext _contexto = new ClienteContext();
        public IUnitOfWork UnitOfWork => _contexto;

        public Solicitud Add(Solicitud solicitud)
        {
            _contexto.Solicitudes.Add(solicitud);
            return solicitud;
        }

        public void Update(Solicitud solicitud)
        {
            _contexto.Entry(solicitud).State =
                        EntityState.Modified;
        }

        public void Delete(Solicitud solicitud)
        {
            _contexto.Solicitudes.Remove(solicitud);
        }        

        public Solicitud GetSolicitud(int id)
        {
            var solicitud = _contexto.Solicitudes.Find(id);
            if (solicitud != null)
            {
                _contexto.Entry(solicitud)
                    .Reference(s => s.Unidad).Load();
            }
            return solicitud;
        }

        public UnidadDeNegocio GetUnidadDeNegocio(int id)
        {
            var unidad = _contexto.Unidades.Find(id);
            if (unidad != null)
            {
                _contexto.Entry(unidad)
                    .Reference(un => un.Cliente).Load();

                _contexto.Entry(unidad)
                    .Collection(un => un.Direcciones).Load();
            }
            return unidad;
        }       

        public List<Solicitud> ListSolicitud()
        {
            return _contexto.Solicitudes.ToList();
        }

        public List<Solicitud> ListSolicitudPorUnidad(int id)
        {
            return _contexto.Solicitudes.Where(s => s.IDUnidadDeNegocio==id).ToList();
        }

    }
}
