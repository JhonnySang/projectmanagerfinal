﻿using EscuelaNet.Dominio.Capacitaciones;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EscuelaNet.Infraestructura.Capacitaciones.Repositorios
{
    public class InstructoresRepository : IInstructorRepository
    {
        private CapacitacionRepository _contexto = new CapacitacionRepository();
        public IUnitOfWork UnitOfWork => _contexto;

        public Instructor Add(Instructor instructor)
        {
            _contexto.Instructores.Add(instructor);
            return instructor;
        }

        public void Delete(Instructor instructor)
        {
            _contexto.Instructores.Remove(instructor);
        }

        public Instructor GetInstructor(int id)
        {
            var instructor = _contexto.Instructores.Find(id);
            if (instructor != null)
            {
                _contexto.Entry(instructor)
                    .Collection(i => i.Temas).Load();
            }

            return instructor;
        }

        public List<Instructor> ListInstructores()
        {
            return _contexto.Instructores.ToList();
        }

        public void Update(Instructor instructor)
        {
            _contexto.Entry(instructor).State = EntityState.Modified;
        }
    }
}
