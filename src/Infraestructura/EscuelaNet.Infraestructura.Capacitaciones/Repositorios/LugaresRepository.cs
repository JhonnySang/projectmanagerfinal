﻿using EscuelaNet.Dominio.Capacitaciones;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Capacitaciones.Repositorios
{
    public class LugaresRepository : ILugarRepository
    {
        private CapacitacionRepository _contexto = new CapacitacionRepository();
        public IUnitOfWork UnitOfWork => _contexto;

        public Lugar Add(Lugar lugar)
        {
            _contexto.Lugares.Add(lugar);
            return lugar;
        }

        public void Delete(Lugar lugar)
        {
            _contexto.Lugares.Remove(lugar);
        }

        public Lugar GetLugar(int id)
        {
            var lugar = _contexto.Lugares.Find(id);
            if (lugar != null)
            {
                _contexto.Entry(lugar);
            }
            return lugar;
        }

        public List<Lugar> ListLugar()
        {
            return _contexto.Lugares.ToList();
        }

        public void Update(Lugar lugar)
        {
            _contexto.Entry(lugar).State = EntityState.Modified;
        }
    }
}
