USE [AdministradorDeProyectos]
GO

/****** Object:  Table [dbo].[Programador]    Script Date: 11/09/2019 12:24:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Programador](
	[IdProgramador] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](75) NOT NULL,
	[Apellido] [varchar](75) NOT NULL,
	[Legajo] [varchar](75) NOT NULL,
	[Dni] [varchar](75) NOT NULL,
	[Rol] [varchar](75) NOT NULL,
	[FechaNacimiento] [datetime] NOT NULL,
	[Disponibilidad] [int] NOT NULL,
	[Equipo_ID] [int] NOT NULL,
 CONSTRAINT [PK__Programa__28EB8867CFE2BBDE] PRIMARY KEY CLUSTERED 
(
	[IdProgramador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


