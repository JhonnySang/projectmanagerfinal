﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EscuelaNet.Dominio.Programadores.Test
{
    [TestClass]
    public class UnitTestProgramadores
    {
        [TestMethod]
        public void PROBAR_CREAR_UN_EQUIPO()
        {
            var programador = new Programador("Cristian", "Martinez", "42201", "43501357", "Programador", DateTime.Now);
            Assert.AreEqual(EstadoDeDisponibilidad.PartTime.ToString(), programador.ConsultarDisponibilidad());

            var conocimiento = new Skills("Java", Experiencia.Junior);
            var conocmiento1 = new Skills("Python", Experiencia.Senior);

            programador.PushConocimiento(conocimiento);
            programador.PushConocimiento(conocmiento1);

            Action accion = () =>
            {
                var equipo = new Equipo("Alfa", "Italia", -13);
                var conocimientoequipo = new Skills("Python", Experiencia.Junior);
                equipo.PushSkill(conocimientoequipo);
                equipo.PushProgramador(programador);
            };
            Assert.ThrowsException<ExcepcionDeEquipo>(accion);

        }

        [TestMethod]
        public void PROBAR_CONSULTA_DISPONIBILIDAD_PROGRAMADOR()
        {
            var programador = new Programador("Cristian", "Martinez", "42201", "43501357", "Programador", DateTime.Now);
            programador.CambiarDisponibilidad(EstadoDeDisponibilidad.FullTime);
            Assert.AreEqual(EstadoDeDisponibilidad.FullTime.ToString(), programador.ConsultarDisponibilidad());
            programador.CambiarDisponibilidad(EstadoDeDisponibilidad.PartTime);
            Assert.AreEqual(EstadoDeDisponibilidad.FullTime.ToString(), programador.ConsultarDisponibilidad());
        }

        [TestMethod]
        public void PROBAR_CANTIDAD_DE_PROGRAMADORES()
        {
            var programador = new Programador("Cristian", "Martinez", "42201", "43501357", "Programador", DateTime.Now);
            var programador1 = new Programador("Alexis", "Medrano", "42206", "35811528", "Programador", DateTime.Now);
            var programador2 = new Programador("Gabriel", "Rios", "42224", "39728526", "Programador", DateTime.Now);

            programador1.CambiarDisponibilidad(EstadoDeDisponibilidad.FullTime);
            programador2.CambiarDisponibilidad(EstadoDeDisponibilidad.SinDisponiblilidad);

            var equipo = new Equipo("Alfa", "Italia", -11);

            var conocimientoequipo = new Skills(".Net", Experiencia.Junior);
            equipo.PushSkill(conocimientoequipo);

            var conocimiento = new Skills(".Net", Experiencia.Junior);

            programador.PushConocimiento(conocimiento);
            programador1.PushConocimiento(conocimiento);
            programador2.PushConocimiento(conocimiento);

            equipo.PushProgramador(programador);
            equipo.PushProgramador(programador1);
            equipo.PushProgramador(programador2);

            Assert.AreEqual(equipo.CantidadHorasDisponibleProgramadores(), 12);
            Assert.AreEqual(equipo.CantidadProgramadores, 3);

        }

        [TestMethod]
        public void PROBAR_REMOVE_PROGRAMADORES()
        {
            var programador = new Programador("Cristian", "Martinez", "42201", "43501357", "Programador", DateTime.Now);
            var programador1 = new Programador("Alexis", "Medrano", "42206", "35811528", "Programador", DateTime.Now);
            var programador2 = new Programador("Gabriel", "Rios", "42224", "39728526", "Programador", DateTime.Now);

            var equipo = new Equipo("Alfa", "Italia", -11);

            var conocimientoequipo = new Skills(".Net", Experiencia.Junior);
            equipo.PushSkill(conocimientoequipo);

            var conocimiento = new Skills(".Net", Experiencia.Junior);

            programador.PushConocimiento(conocimiento);
            programador1.PushConocimiento(conocimiento);
            programador2.PushConocimiento(conocimiento);

            equipo.PushProgramador(programador);
            equipo.PushProgramador(programador1);
            equipo.PushProgramador(programador2);

            Assert.AreEqual(equipo.CantidadHorasDisponibleProgramadores(), 12);

            equipo.RemoveProgramador(1);

            Assert.AreEqual(equipo.CantidadProgramadores, 2);

        }

        [TestMethod]
        public void PROBAR_EDIT_PROGRAMADORES()
        {
            var programador = new Programador("Cristian", "Martinez", "42201", "43501357", "Programador", DateTime.Now);

            var conocimiento = new Skills(".Net", Experiencia.Junior);

            programador.PushConocimiento(conocimiento);

            var equipo = new Equipo("Alfa", "Italia", -11);
            var conocimientoequipo = new Skills(".Net", Experiencia.Junior);
            equipo.PushSkill(conocimientoequipo);

            equipo.PushProgramador(programador);

            programador.EditProgramadores(programador, "Esteban", "Martinez", "42206", "12345678", "Testeador", DateTime.Now);

            Assert.AreEqual(programador.Nombre, "Esteban");
        }
    }
}
