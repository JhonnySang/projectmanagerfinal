﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Programadores
{
    public class Equipo : Entity, IAggregateRoot
    {
        public string Nombre { get; set; }
        public string Pais { get; set; }
        public int HusoHorario { get; set; }
        public int CantidadProgramadores { get; set; }
        public IList<Programador> Programadores { get; set; }
        public IList<Skills> Skills { get; set; }

        public Equipo(string nombre,string pais,int hora)
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
            this.Pais = pais ?? throw new System.ArgumentNullException(nameof(pais));
            this.HusoHorario = VerificarHusoHorario(hora);
            this.CantidadProgramadores = 0;
        }
        
        private Equipo() {}


        public void PushProgramador(Programador programador)
        {
            if(this.Programadores == null)
            {
                this.Programadores = new List<Programador>();  
            }
            //foreach (var conocimiento in this.Skills)
            //{
            //    if (programador.Skills == null || programador.Skills.Contains(conocimiento))
            //    {
            //        throw new Exception("No contiene los conocimientos del equipo");
            //    }
            //}
            this.CantidadProgramadores += 1;
            this.Programadores.Add(programador);    
        }

        public double CantidadHorasDisponibleProgramadores()
        {
            var countFull = 0;
            var countPart = 0;
            var countSinDisponibilidad = 0;

            if (this.Programadores == null)
            {
                return 0;
            }

            foreach(var programadores in this.Programadores)
            {

                switch (programadores.Disponibilidad)
                {
                    case EstadoDeDisponibilidad.FullTime:
                        countFull += 1;
                        break;
                    case EstadoDeDisponibilidad.PartTime:
                        countPart += 1;
                        break;
                    case EstadoDeDisponibilidad.SinDisponiblilidad:
                        countSinDisponibilidad += 1;
                        break;
                    default:
                        throw new ExcepcionDeEquipo("No existe un estado de disponibilidad para el programador");
                        
                }
            }
            var totalProgramadoresFullTime = countFull * 1 * 8;
            var totalProgramadoresPartTime = countPart * 0.5 * 8;
            var totalProgramadoresSinDisponibilidad = countSinDisponibilidad * 0 * 0;

            var totalProgramadores = totalProgramadoresFullTime + totalProgramadoresPartTime + totalProgramadoresSinDisponibilidad;
            return totalProgramadores;
        }

        public void RemoveProgramador(int id)
        {
            if(id != -1 && id <= Programadores.Count)
            {
                this.Programadores.RemoveAt(id);
                this.CantidadProgramadores -= 1;

            }
            else
            {
                throw new ExcepcionDeEquipo("No existe el programador para eliminar");
            }
        }

        public void PushSkill(Skills skills)
        {
            if (this.Skills == null)
            {
                this.Skills = new List<Skills>();
            }
            this.Skills.Add(skills);
        }

        public int VerificarHusoHorario(int husohorario)
        {
            if(husohorario >= -12 && husohorario <= 14)
            {
                return husohorario;
            }
            else
            {
                throw new ExcepcionDeEquipo("El HusoHorario no esta en el rango establecido");
            }
            
        }
        
    }
}