﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Clientes
{
    public class Solicitud : Entity, IAggregateRoot
    {
        public string  Titulo { get; set; }

        public string Descripcion { get; set; }

        public EstadoSolicitud Estado { get; set; }

        public UnidadDeNegocio Unidad { get; set; }

        public int? IDUnidadDeNegocio { get; set; }

        private Solicitud() { }

        public Solicitud(string titulo, string descripcion, int idUnidad = 0)
        {
            this.Titulo = titulo ?? throw new System.ArgumentNullException(nameof(titulo));
            this.Descripcion = descripcion ?? throw new System.ArgumentNullException(nameof(descripcion));
            this.Estado = EstadoSolicitud.Borrador;
            if (idUnidad!=0)
            {
                IDUnidadDeNegocio = idUnidad;
            }
        }

        public void CambiarEstado(EstadoSolicitud estadoNuevo)
        {
            if (this.Estado == EstadoSolicitud.Borrador && estadoNuevo == EstadoSolicitud.Desarrollo)
            {
                this.Estado = EstadoSolicitud.Desarrollo;
            }
            else if (this.Estado == EstadoSolicitud.Desarrollo && estadoNuevo == EstadoSolicitud.Produccion)
            {
                this.Estado = EstadoSolicitud.Produccion;
            }
            else if (this.Estado == EstadoSolicitud.Produccion && estadoNuevo == EstadoSolicitud.Deprecado)
            {
                this.Estado = EstadoSolicitud.Deprecado;
            }
            else if (this.Estado == EstadoSolicitud.Desarrollo && estadoNuevo == EstadoSolicitud.Abandonado)
            {
                this.Estado = EstadoSolicitud.Abandonado;
            }
            else if (this.Estado == EstadoSolicitud.Abandonado && estadoNuevo == EstadoSolicitud.Borrador)
            {
                this.Estado = EstadoSolicitud.Borrador;
            }
            else if (this.Estado == estadoNuevo)
            {
                this.Estado = estadoNuevo;
            }
            else
            {
                string mensaje = "No se puede pasar una solicitud con estado "+this.Estado.ToString()+" al estado "+estadoNuevo.ToString();
                throw new ExcepcionDeCliente(mensaje);
            }
        }
    }
}
