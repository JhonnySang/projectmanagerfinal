﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Clientes
{
    public interface ISolicitudRepository : IRepository<Solicitud>
    {
        Solicitud Add(Solicitud solicitud);
        void Update(Solicitud solicitud);
        void Delete(Solicitud solicitud);       
        Solicitud GetSolicitud(int id);

        /// <summary>
        /// Trae una unidad por su primary key
        /// </summary>
        /// <param name="id">Id unico de la unidad</param>
        /// <returns></returns>
        UnidadDeNegocio GetUnidadDeNegocio(int id);        
        List<Solicitud> ListSolicitud();

        List<Solicitud> ListSolicitudPorUnidad(int id);
    }
}
