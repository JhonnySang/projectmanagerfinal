using EscuelaNet.Dominio.SeedWoork;
using System.Collections.Generic;
using System.Linq;

namespace EscuelaNet.Dominio.Clientes
{
    public class UnidadDeNegocio : Entity
    {
        public string RazonSocial { get; set; }

        public string ResponsableDeUnidad { get; set; }

        public string Cuit { get; set; }

        public string EmailResponsable { get; set; }

        public string TelefonoResponsable { get; set; }

        public IList<Direccion> Direcciones { get; private set; }

        public IList<Solicitud> Solicitudes { get; set; }

        public Cliente Cliente { get; set; }

        public int IDCliente { get; set; }

        private UnidadDeNegocio() { }

        public UnidadDeNegocio(string razonSocial, string responsable, string cuit, string email, string telefono)
        {
            this.RazonSocial = razonSocial ?? throw new System.ArgumentNullException(nameof(razonSocial));
            this.ResponsableDeUnidad = responsable ?? throw new System.ArgumentNullException(nameof(responsable));
            this.Cuit = cuit ?? throw new System.ArgumentNullException(nameof(cuit));
            this.EmailResponsable = email ?? throw new System.ArgumentNullException(nameof(email));
            this.TelefonoResponsable = telefono ?? throw new System.ArgumentNullException(nameof(telefono));
           
        }

        public void AgregarDireccion(Direccion direccion)
        {
            if (this.Direcciones == null)
            {
                this.Direcciones = new List<Direccion>();
            }
            if (!this.Direcciones.Any(x => x.toString() == direccion.toString()))
            {
                this.Direcciones.Add(direccion);
            }
        }

        public double HumorUnidad()
        {
            var cantidadSolicitudesTotal = 0;
            var cantidadSolicitudesBD = 0;                     
               
            if (this.Solicitudes != null)
            {
                foreach (var solicitud in this.Solicitudes)
                {
                    if (solicitud.Estado != EstadoSolicitud.Abandonado)
                    {                    
                        cantidadSolicitudesTotal++;

                        if (solicitud.Estado == EstadoSolicitud.Borrador
                            || solicitud.Estado == EstadoSolicitud.Desarrollo )
                        {
                            cantidadSolicitudesBD++;
                        }
                    }
                }
            }               

            if (cantidadSolicitudesTotal == 0)
            {
                return 100.0;
            }
            else
            {
                var humor = 100 * (cantidadSolicitudesTotal - cantidadSolicitudesBD) / cantidadSolicitudesTotal;
                return humor;
            }

           
        }

        public void AgregarSolicitud(Solicitud solicitud)
        {
            if (this.Solicitudes == null)
            {
                this.Solicitudes = new List<Solicitud>();
            }
            if (!this.Solicitudes.Any(x => x.Titulo == solicitud.Titulo))
            {
                if (this.HumorUnidad()<20 && this.Solicitudes.Count>3)
                {
                    throw new ExcepcionDeCliente("Humor demasiado bajo para poder generar una nueva solicitud");
                }
                else
                {
                    this.Solicitudes.Add(solicitud);
                }
                
            }
        }

    }
}
