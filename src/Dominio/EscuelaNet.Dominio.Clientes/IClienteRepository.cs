﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Clientes
{
    public interface IClienteRepository : IRepository<Cliente>
    {
        Cliente Add(Cliente cliente);
        void Update(Cliente cliente);
        void Delete(Cliente cliente);
        void DeleteUnidadDeNegocio(UnidadDeNegocio unidad);
        void DeleteDireccion(Direccion direccion);
        Cliente GetCliente(int id);


        /// <summary>
        /// Trae una unidad por su primary key
        /// </summary>
        /// <param name="id">Id unico de la unidad</param>
        /// <returns></returns>
        UnidadDeNegocio GetUnidadDeNegocio(int id);

        Direccion GetDireccion(int id);
        List<Cliente> ListCliente();
    }
}
