﻿using System;
using EscuelaNet.Dominio.Conocimientos;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestConocimientos
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void PROBAR_INGRESAR_PRECIO_NEGATIVO()
        {
            Action action = () =>
            {
                var ConocimientoPorPrecio = new ConocimientoPorPrecio(10, null, Nivel.Basico);

            };
            Assert.ThrowsException<ArgumentException>(action);

            Action action1 = () =>
            {
                var ConocimientoPorPrecio1 = new ConocimientoPorPrecio(-10, "Dolar", Nivel.Basico);

            };
            Assert.ThrowsException<ExcepcionDeConocimiento>(action1);
        }

        [TestMethod]
        public void PROBAR_INGRESAR_CATEGORIA_SIN_NOMBRE()
        {
            Action action2 = () =>
            {
                var Categoria = new Categoria(null, "Algorespecto a la categoria");

            };
            Assert.ThrowsException<ArgumentNullException>(action2);
        }

        [TestMethod]
        public void PROBAR_INGRESAR_ASESOR_SIN_DATOS_PERSONALES()
        {
            Action action3 = () =>
            {
                var Asesor = new Asesor(null, null, null, null);

            };
            Assert.ThrowsException<ArgumentNullException>(action3);
        }

        public void PROBAR_AGREGAR_ASESOR_LISTA()
        {
            var conocimiento = new Conocimiento("java");
            conocimiento.AgregarAsesor("Juan", "Perez", "Ingles", "Argentina");
            Assert.AreEqual("Juan", conocimiento.Asesores[0].Nombre);
            Assert.AreEqual("Perez", conocimiento.Asesores[0].Apellido);
            Assert.AreEqual("Ingles", conocimiento.Asesores[0].Idioma);
            Assert.AreEqual("Argentina", conocimiento.Asesores[0].Pais);
        }

        public void PROBAR_INGRESAR_PRECIO_CONOCMIENTO()
        {
            var conocimiento = new Conocimiento("Java");
            conocimiento.AgregarConocimientoPorPrecio(Nivel.Avanzado, 200, "Dolar");
            Assert.AreEqual(Nivel.Avanzado, conocimiento.ConocimientoPorprecio[0].Nivel);
            Assert.AreEqual(200, conocimiento.ConocimientoPorprecio[0].ValorNominal);
            Assert.AreEqual("Dolar", conocimiento.ConocimientoPorprecio[0].Moneda);
        }

        [TestMethod]
        public void PROBAR_COMPARAR_TRIMESTRE_ANTERIOR()
        {
            var conocimientoporprecio = new ConocimientoPorPrecio(200, "Dolar", new DateTime(2019, 3, 2));
            var cpanterior = new ConocimientoPorPrecio(170, "Dolar", new DateTime(2019, 5, 3));
            conocimientoporprecio.TrimestreAnterior(conocimientoporprecio, cpanterior);
        }

        [TestMethod]
        public void PROBAR_INFLACION_ANUAL()
        {
            var conocimientoporprecio = new ConocimientoPorPrecio(200, "Dolar", new DateTime(2019, 1, 1));
            var cpanterior = new ConocimientoPorPrecio(170, "Dolar", new DateTime(2018, 1, 1));
            double porcentaje = 5;
            conocimientoporprecio.InflacionAnual(conocimientoporprecio, cpanterior, porcentaje);


        }

    }
}
