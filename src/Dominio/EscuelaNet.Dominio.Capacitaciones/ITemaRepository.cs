﻿using EscuelaNet.Dominio.Clientes;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public interface ITemaRepository : IRepository<Tema>
    {
        Tema Add(Tema tema);
        void Update(Tema tema);
        void Delete(Tema tema);
        Tema GetTema(int id);

        List<Tema> ListTemas();
        

    }
}
