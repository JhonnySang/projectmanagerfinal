﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EscuelaNet.Dominio.SeedWoork;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public interface IInstructorRepository : IRepository<Instructor>
    {
        Instructor Add(Instructor instructor);
        void Update(Instructor instructor);
        void Delete(Instructor instructor);
        Instructor GetInstructor(int id);

        List<Instructor> ListInstructores();


    }
}

