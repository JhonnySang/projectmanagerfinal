﻿using EscuelaNet.Dominio.Clientes;
//using EscuelaNet.Presentacion.Clientes.Web.Infraestructura;
using EscuelaNet.Presentacion.Clientes.Web.Models;
using EsculaNet.Infraestructura.Clientes;
using EsculaNet.Infraestructura.Clientes.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Clientes.Web.Controllers
{
    public class UnidadesController : Controller
    {
        private IClienteRepository Repositorio = new ClientesRepository();
        public ActionResult Index(int id)
        {
            var cliente = Repositorio.GetCliente(id);           
            if (cliente.Unidades==null)
            {
                TempData["error"] = "Cliente sin unidades";
                return RedirectToAction("../Clientes/Index");
            }
            var model = new UnidadesIndexModel()
            {
                Titulo = "Unidades del Cliente '" + cliente.RazonSocial+"'",
                Unidades = cliente.Unidades.ToList(),
                IdCliente = id
            };

            return View(model);
        }

        public ActionResult New(int id)
        {
            var cliente = Repositorio.GetCliente(id);
            var model = new NuevaUnidadModel()
            {
                Titulo = "Nueva unidad para el Cliente '"+cliente.RazonSocial+"'",
                IdCliente = id
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult New(NuevaUnidadModel model)
        {
            if (!string.IsNullOrEmpty(model.RazonSocial))
            {
                try
                {                    
                    var razonSocial = model.RazonSocial;
                    var responsable = model.ResponsableDeUnidad;
                    var cuit = model.Cuit;
                    var email = model.EmailResponsable;
                    var telefono = model.TelefonoResponsable;

                    var cliente = Repositorio.GetCliente(model.IdCliente);
                    
                    var unidad = new UnidadDeNegocio(razonSocial, responsable, cuit, email, telefono);
                    cliente.AgregarUnidad(unidad);
                    Repositorio.Update(cliente);
                    Repositorio.UnitOfWork.SaveChanges();
                    TempData["success"] = "Unidad creada";
                    return RedirectToAction("Index/"+model.IdCliente);

                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }
        // "/Unidades/Edit/1?idUnidad=10
        public ActionResult Edit(int id)
        {

            if (id <= 0)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var unidad = Repositorio.GetUnidadDeNegocio(id);              

                var model = new NuevaUnidadModel()
                {
                    Titulo="Editar la Unidad '"+unidad.RazonSocial+"' del Cliente '"+ unidad.Cliente.RazonSocial+"'",
                    IdCliente = unidad.Cliente.ID,
                    IdUnidad = id,
                    RazonSocial = unidad.RazonSocial,
                    ResponsableDeUnidad = unidad.ResponsableDeUnidad,
                    EmailResponsable = unidad.EmailResponsable,
                    Cuit = unidad.Cuit,
                    TelefonoResponsable = unidad.TelefonoResponsable,
                                       
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(NuevaUnidadModel model)
        {
            if (!string.IsNullOrEmpty(model.RazonSocial))
            {
                try
                {
                    var cliente = Repositorio.GetCliente(model.IdCliente);                    
                    cliente.Unidades.Where(un=>un.ID == model.IdUnidad).First().RazonSocial = model.RazonSocial;
                    cliente.Unidades.Where(un=>un.ID == model.IdUnidad).First().ResponsableDeUnidad = model.ResponsableDeUnidad;
                    cliente.Unidades.Where(un=>un.ID == model.IdUnidad).First().Cuit = model.Cuit;
                    cliente.Unidades.Where(un=>un.ID == model.IdUnidad).First().EmailResponsable = model.EmailResponsable;
                    cliente.Unidades.Where(un=>un.ID == model.IdUnidad).First().TelefonoResponsable = model.TelefonoResponsable;

                    Repositorio.Update(cliente);
                    Repositorio.UnitOfWork.SaveChanges();
                    TempData["success"] = "Unidad editada";
                    return RedirectToAction("Index/"+model.IdCliente);
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            if (id < 0)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var unidad = Repositorio.GetUnidadDeNegocio(id);                
                var model = new NuevaUnidadModel()
                {
                    Titulo = "Borrar la Unidad '" + unidad.RazonSocial + "' del Cliente '" + unidad.Cliente.RazonSocial + "'",
                    IdCliente = unidad.Cliente.ID,
                    IdUnidad = id,
                    RazonSocial = unidad.RazonSocial,
                    ResponsableDeUnidad = unidad.ResponsableDeUnidad,
                    EmailResponsable = unidad.EmailResponsable,
                    Cuit = unidad.Cuit,
                    TelefonoResponsable = unidad.TelefonoResponsable,
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Delete(NuevaUnidadModel model)
        {

            try
            {
                var unidad = Repositorio.GetUnidadDeNegocio(model.IdUnidad);
                Repositorio.DeleteUnidadDeNegocio(unidad);
                Repositorio.UnitOfWork.SaveChanges();
                TempData["success"] = "Unidad borrada";
                return RedirectToAction("../Unidades/Index/"+model.IdCliente);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }

        }

    }
}