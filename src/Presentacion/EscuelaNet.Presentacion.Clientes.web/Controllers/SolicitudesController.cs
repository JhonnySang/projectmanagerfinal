﻿using EscuelaNet.Dominio.Clientes;
//using EscuelaNet.Presentacion.Clientes.Web.Infraestructura;
using EscuelaNet.Presentacion.Clientes.Web.Models;
using EsculaNet.Infraestructura.Clientes.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Clientes.Web.Controllers
{
    public class SolicitudesController : Controller
    {
        private ISolicitudRepository Repositorio = new SolicitudesRepository();
        //private IClienteRepository RepositorioCliente = new ClientesRepository();

        public ActionResult Index(int? id)
        {
            
            if (id != null)
            {
                int idUnidad = (int) id;
                var solicitudes = Repositorio.ListSolicitudPorUnidad(idUnidad);
                var model = new SolicitudesIndexModel()
                {
                    Titulo = "Solicitudes de la Unidad numero: "+idUnidad,
                    Solicitudes = solicitudes,
                    IdUnidad = id
                };

                return View(model);
            }
            else
            {
                var solicitudes = Repositorio.ListSolicitud();                
                var model = new SolicitudesIndexModel()
                {
                    Titulo = "Solicitudes",
                    Solicitudes = solicitudes,                    
                };

                return View(model);
            }               
        }

        public ActionResult New(int? id)
        {
            if (id!=null)
            {
                int idUnidad = (int)id;
                var model = new NuevaSolicitudModel()
                {
                    Title = "Nueva Solicitud para la Unidad numero: "+idUnidad,
                    IdUnidad = idUnidad
                };
                return View(model);
            }
            else
            {
                var model = new NuevaSolicitudModel()
                {
                    Title = "Nueva Solicitud ",
                };
                return View(model);
            }            
        }

        [HttpPost]
        public ActionResult New(NuevaSolicitudModel model)
        {
            if (!string.IsNullOrEmpty(model.Titulo))
            {
                try
                {
                    var solicitud = new Solicitud(model.Titulo, model.Descripcion, model.IdUnidad);
                    Repositorio.Add(solicitud);
                    Repositorio.UnitOfWork.SaveChanges();

                    //----------------------------prueba---------------------
                        //  if (model.IdUnidad!=0)
                        //  {
                        //      var unidad = RepositorioCliente.GetUnidadDeNegocio(model.IdUnidad);
                        //      unidad.AgregarSolicitud(solicitud);
                        //      RepositorioCliente.Update(unidad.Cliente);
                        //      RepositorioCliente.UnitOfWork.SaveChanges();
                        //  }
                    //-------------------------------------------------------


                    TempData["success"] = "Solicitud creada";
                    if (model.IdUnidad == 0)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return RedirectToAction("Index/", new { id = model.IdUnidad });
                    }
                        

                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            if (id <= 0)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var solicitud = Repositorio.GetSolicitud(id);
              
                var model = new NuevaSolicitudModel()
                {
                    Title = "Editar la solicitud " + solicitud.Titulo,
                    IdSolicitud = id,
                    Titulo = solicitud.Titulo,
                    Descripcion = solicitud.Descripcion,
                    Estado = solicitud.Estado
                };

                return View(model);
            }
            
        }

        [HttpPost]
        public ActionResult Edit(NuevaSolicitudModel model)
        {
            if (!string.IsNullOrEmpty(model.Titulo))
            {
                try
                {
                    var solicitud = Repositorio.GetSolicitud(model.IdSolicitud);
                    solicitud.Titulo = model.Titulo;
                    solicitud.Descripcion = model.Descripcion;
                    solicitud.CambiarEstado(model.Estado);

                    Repositorio.Update(solicitud);
                    Repositorio.UnitOfWork.SaveChanges();
                    TempData["success"] = "Solicitud editada";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            if (id <= 0)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var solicitud = Repositorio.GetSolicitud(id);

                var model = new NuevaSolicitudModel()
                {
                    IdSolicitud = id,
                    Titulo = solicitud.Titulo,
                    Descripcion = solicitud.Descripcion,
                    Estado = solicitud.Estado
                };

                return View(model);
            }      
            
        }

        [HttpPost]
        public ActionResult Delete(NuevaSolicitudModel model)
        {

            try
            {
                var solicitud = Repositorio.GetSolicitud(model.IdSolicitud);
                Repositorio.Delete(solicitud);
                Repositorio.UnitOfWork.SaveChanges();
                TempData["success"] = "Solicitud borrada";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }

        }

    }
}