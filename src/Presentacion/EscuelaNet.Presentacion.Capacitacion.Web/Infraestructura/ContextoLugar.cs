﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Web;
using EscuelaNet.Dominio.Capacitaciones;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Infraestructura
{
    public sealed class ContextoLugar
    {
        private static ContextoLugar _instancia = new ContextoLugar();

        public List<Lugar> Lugares { get; set; }

        private ContextoLugar()
        {
            this.Lugares = new List<Lugar>();
            this.Lugares.Add(new Lugar(20,"Las Piedras","123","","","San Miguel de Tucuman","Tucuman","Argentina"));
            this.Lugares.Add(new Lugar(20, "San Lorenzo", "3234", "", "", "Yerba Buena", "Tucuman", "Argentina"));
        }

        public static ContextoLugar Instancia
        {
            get { return _instancia; }
        }
    }
}