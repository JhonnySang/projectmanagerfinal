﻿using EscuelaNet.Dominio.Capacitaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Models
{
    public class NuevoInstructorTemaModel
    {
        public int IDTema { get; set; }
        public int IDInstructor { get; set; }
        public string NombreTema { get; set; }
        public string NombreApellido { get; set; }
        public List<Tema> Temas { get;  set; }
    }
}