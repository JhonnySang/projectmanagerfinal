﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EscuelaNet.Dominio.Capacitaciones;
using EscuelaNet.Infraestructura.Capacitaciones.Repositorios;
using EscuelaNet.Presentacion.Capacitacion.Web.Models;
using Microsoft.Ajax.Utilities;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Controllers
{
    public class LugaresController : Controller
    {
        private ILugarRepository Repositorio = new LugaresRepository();
        // GET: Lugares
        public ActionResult Index()
        {
            var lugares = Repositorio.ListLugar();
            var model = new LugarIndexModel()
            {
                Titulo = "Lugares",
                Lugares = lugares
            };

            return View(model);
        }


        // GET: Lugares/Create
        public ActionResult New()
        {
            var model = new NuevoLugarModel();
            return View(model);
        }

        // POST: Lugares/Create
        [HttpPost]
        public ActionResult New(NuevoLugarModel model)
        {
            if (model.Capacidad > 0)
            {
                if (string.IsNullOrEmpty(model.Depto))
                {
                    model.Depto = " ";
                }

                if (string.IsNullOrEmpty(model.Piso))
                {
                    model.Piso = " ";
                }

                Repositorio.Add(new Lugar(model.Capacidad, model.Calle, model.Numero
                    , model.Depto, model.Piso, model.Localidad, model.Provincia, model.Pais));
                Repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Lugar Agregado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = "Error, La Capacidad debe ser MAYOR a 0";
                return View(model);
            }
        }

        // GET: Lugares/Edit/5
        public ActionResult Edit(int id)
        {
            var lugar = Repositorio.GetLugar(id);

            var model = new NuevoLugarModel()
            {
                Capacidad = lugar.Capacidad,
                Id = id,
                Calle = lugar.Calle,
                Numero = lugar.Numero,
                Depto = lugar.Depto,
                Piso = lugar.Piso,
                Localidad = lugar.Localidad,
                Provincia = lugar.Provincia,
                Pais = lugar.Pais,
            };
            return View(model);
        }

        // POST: Lugares/Edit/5
        [HttpPost]
        public ActionResult Edit(NuevoLugarModel model)
        {
            if (model.Capacidad > 0)
            {
                if (string.IsNullOrEmpty(model.Depto))
                {
                    model.Depto = " ";
                }

                if (string.IsNullOrEmpty(model.Piso))
                {
                    model.Piso = " ";
                }

                try
                {
                    var lugar = Repositorio.GetLugar(model.Id);
                    lugar.Calle = model.Calle;
                    lugar.Capacidad = model.Capacidad;
                    lugar.Depto = model.Depto;
                    lugar.Localidad = model.Localidad;
                    lugar.Numero = model.Numero;
                    lugar.Pais = model.Pais;
                    lugar.Piso = model.Piso;
                    lugar.Provincia = model.Provincia;


                    Repositorio.Update(lugar);
                    Repositorio.UnitOfWork.SaveChanges();

                    TempData["success"] = "Lugar Editado";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Capacidad Debe Ser MAYOR a 0";
                return View(model);

            }
        }

        // GET: Lugares/Delete/5
        public ActionResult Delete(int id)
        {
            var lugar = Repositorio.GetLugar(id);

            var model = new NuevoLugarModel()
            {
                Capacidad = lugar.Capacidad,
                Id = id,
                Calle = lugar.Calle,
                Numero = lugar.Numero,
                Depto = lugar.Depto,
                Piso = lugar.Piso,
                Localidad = lugar.Localidad,
                Provincia = lugar.Provincia,
                Pais = lugar.Pais,
            };
            return View(model);
        }

        // POST: Lugares/Delete/5
        [HttpPost]
        public ActionResult Delete(NuevoLugarModel model)
        {
            try
            {
                var lugar = Repositorio.GetLugar(model.Id);

                Repositorio.Delete(lugar);

                Repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Lugar borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
    }
}
