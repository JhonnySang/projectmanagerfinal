﻿using EscuelaNet.Dominio.Capacitaciones;
using EscuelaNet.Infraestructura.Capacitaciones;
using EscuelaNet.Infraestructura.Capacitaciones.Repositorios;
using EscuelaNet.Presentacion.Capacitacion.Web.Infraestructura;
using EscuelaNet.Presentacion.Capacitacion.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Controllers
{
    public class TemasController : Controller
    {

        private ITemaRepository Repositorio = new TemasRepository();
        // GET: Tema
        public ActionResult Index()
        {
            var temas = Repositorio.ListTemas();

            var model = new TemaIndexModel()
            {
                Titulo = "Primera prueba",
                Temas = temas
            };
            return View(model);
        }

        // GET: Tema/New
        public ActionResult New()
        {
            var model = new NuevoTemaModel();
            return View(model);
        }
        // POST: Tema/New
        [HttpPost]
        public ActionResult New(NuevoTemaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    Repositorio.Add(new Tema(model.Nombre, model.Nivel));
                    Repositorio.UnitOfWork.SaveChanges();

                    TempData["success"] = "Tema creado";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }
        }

        // GET: Tema/Edit/5
        public ActionResult Edit(int id)
        {
            var tema = Repositorio.GetTema(id);

            var model = new NuevoTemaModel()
            {
                Nombre = tema.Nombre,
                Id = id,
                Nivel = tema.Nivel
            };
            return View(model);
        }

        // POST: Tema2/Edit/5
        [HttpPost]
        public ActionResult Edit(NuevoTemaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var tema = Repositorio.GetTema(model.Id);
                    tema.Nombre = model.Nombre;
                    tema.Nivel = model.Nivel;

                    Repositorio.Update(tema);
                    Repositorio.UnitOfWork.SaveChanges();


                    TempData["success"] = "Tema Editado";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }

        }

        // GET: Tema/Delete/5
        public ActionResult Delete(int id)
        {
            var tema = Repositorio.GetTema(id);

            var model = new NuevoTemaModel()
            {
                Nombre = tema.Nombre,
                Id = id,
                Nivel = tema.Nivel,
            };
            return View(model);
        }

        // POST: Tema/Delete/5
        [HttpPost]
        public ActionResult Delete(NuevoTemaModel model)
        {
            try
            {
                var tema = Repositorio.GetTema(model.Id);
                Repositorio.Delete(tema);
                Repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Tema borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
    }
}
