﻿using EscuelaNet.Dominio.Capacitaciones;
//using EscuelaNet.Presentacion.Capacitacion.Web.Infraestructura;
using EscuelaNet.Presentacion.Capacitacion.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EscuelaNet.Infraestructura.Capacitaciones.Repositorios;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Controllers
{
    public class InstructoresController : Controller
    {
        private IInstructorRepository Repositorio = new InstructoresRepository();
        private ITemaRepository RepositorioTema = new TemasRepository();
        // GET: Instructor
        public ActionResult Index()
        {
            var instructores = Repositorio.ListInstructores();

            var model = new InstructorIndexModel()
            {
                Titulo = "Instructores",
                Instructores = instructores,

            };
            return View(model);
        }


        // GET: Instructor/New
        public ActionResult New()
        {
            var model = new NuevoInstructorModel()
            {
                FechaNacimiento = null
            };
            return View(model);
        }

        // POST: Instructor/New
        [HttpPost]
        public ActionResult New(NuevoInstructorModel model)
        {
            if (string.IsNullOrEmpty(model.Nombre)
                || string.IsNullOrEmpty(model.Apellido)
                || string.IsNullOrEmpty(model.Dni)
                || !model.FechaNacimiento.HasValue)
            {

                TempData["error"] = "Nada de Espacios en Blanco";
                return View(model);
            }
            else
            {
                try
                {
                    Repositorio.Add(new Instructor(model.Nombre, model.Apellido,
                        model.Dni, model.FechaNacimiento.Value));

                    Repositorio.UnitOfWork.SaveChanges();

                    TempData["success"] = "Instructor Agregado";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }

        }

        // GET: Instructor/Edit/5
        public ActionResult Edit(int id)
        {
            var instructor = Repositorio.GetInstructor(id);
            var model = new NuevoInstructorModel()
            {
                Nombre = instructor.Nombre,
                Id = id,
                Apellido = instructor.Apellido,
                Dni = instructor.Dni,
                FechaNacimiento = instructor.FechaNacimiento,
            };
            return View(model);
        }

        // POST: Instructor/Edit/5
        [HttpPost]
        public ActionResult Edit(NuevoInstructorModel model)
        {
            if (string.IsNullOrEmpty(model.Nombre)
                || string.IsNullOrEmpty(model.Apellido)
                || string.IsNullOrEmpty(model.Dni)
                || !model.FechaNacimiento.HasValue)
            {

                TempData["error"] = "Nada de espacios en Blanco";
                return View(model);
            }
            else
            {
                try
                {
                    var instructor = Repositorio.GetInstructor(model.Id);
                    instructor.Nombre = model.Nombre;
                    instructor.Apellido = model.Apellido;
                    instructor.Dni = model.Dni;
                    instructor.FechaNacimiento = model.FechaNacimiento.Value;

                    Repositorio.Update(instructor);
                    Repositorio.UnitOfWork.SaveChanges();

                    TempData["success"] = "Instructor Editado";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
        }



        // GET: Instructor/Delete/5
        public ActionResult Delete(int id)
        {
            var instructor = Repositorio.GetInstructor(id);

            var model = new NuevoInstructorModel()
            {
                Nombre = instructor.Nombre,
                Id = id,
                Apellido = instructor.Apellido,
                Dni = instructor.Dni,
                FechaNacimiento = instructor.FechaNacimiento,
            };
            return View(model);
        }

        // POST: Instructor/Delete/5
        [HttpPost]
        public ActionResult Delete(NuevoInstructorModel model)
        {
            try
            {
                var instructor = Repositorio.GetInstructor(model.Id);
                Repositorio.Delete(instructor);
                Repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Instructor borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
        public ActionResult Temas(int id)
        {
            var instructor = Repositorio.GetInstructor(id);
            var model = new InstructorTemaModel()
            {
                Instructor = instructor,
                Temas = instructor.Temas.ToList()
            };
            return View(model);
        }
        public ActionResult DeleteTema(int id, int instructor)
        {
            var instructorBuscado = Repositorio.GetInstructor(instructor);
            var temabuscado = instructorBuscado.Temas.First(t => t.ID == id);
            var model = new NuevoInstructorTemaModel()
            {
                NombreTema = temabuscado.Nombre + " " + temabuscado.Nivel,
                IDInstructor = instructor,
                NombreApellido = instructorBuscado.Nombre + " " + instructorBuscado.Apellido,
                IDTema = id,
            };
            return View(model);
        }

        // POST: Instructor/Delete/5
        [HttpPost]
        public ActionResult DeleteTema(NuevoInstructorTemaModel model)
        {
            try
            {
                var instructorBuscado = Repositorio.GetInstructor(model.IDInstructor);
                var temabuscado = instructorBuscado.Temas.First(t => t.ID == model.IDTema);
                instructorBuscado.pullTema(temabuscado);
                Repositorio.Update(instructorBuscado);
                Repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Instructor borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
        public ActionResult NewTema(int id)
        {
            var instructor = Repositorio.GetInstructor(id);
            var temas = RepositorioTema.ListTemas();
            var model = new NuevoInstructorTemaModel()
            {
                IDInstructor = id,
                NombreApellido = instructor.Nombre + " " + instructor.Apellido,
                Temas = temas
            };
            return View(model);
        }

        // POST: Instructor/New
        [HttpPost]
        public ActionResult NewTema(NuevoInstructorTemaModel model)
        {



            try
            {
                var instructorBuscado = Repositorio.GetInstructor(model.IDInstructor);
                var temabuscado = RepositorioTema.GetTema(model.IDTema);

                instructorBuscado.pushTema(temabuscado);
                Repositorio.Update(instructorBuscado);
                Repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Instructor Agregado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }

        }

    }

}
