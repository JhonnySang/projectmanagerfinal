﻿//using EscuelaNet.Presentacion.Conocimientos.Web.Infraestructura;
using EscuelaNet.Presentacion.Conocimientos.Web.Models;
using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EscuelaNet.Infraestructura.Conocimientos.Repositorios;

namespace EscuelaNet.Presentacion.Conocimientos.Web.Controllers
{
    public class CategoriasController : Controller
    {
        private ICategoriaRepository repositorio = new CategoriaRepositorio();
       // GET: Categoria;
        public ActionResult Index()
        {
            var Categoria = repositorio.ListCategoria();
            var model = new CategoriaIndexModel()
            {
                Titulo = "Primera prueba",
                Categorias = Categoria
            };
            return View(model);
        }

        public ActionResult New()
        {
            var model = new NuevaCategoriaModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult New(NuevaCategoriaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    repositorio.Add(new Dominio.Conocimientos.Categoria(model.Nombre, model.Descripcion));
                    repositorio.UnitOfWork.SaveChanges();
                    TempData["success"] = "Nueva categoria creada correctamente";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }
        }

        public ActionResult Edit(int id)
        {
            var categoria = repositorio.GetCategoria(id);
            var model = new NuevaCategoriaModel()
            {
                Nombre = categoria.Nombre,
                IdCategoria = id,
                Descripcion = categoria.Descripcion
            };
            return View(model);

        }
        [HttpPost] 
        public ActionResult Edit(NuevaCategoriaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var categoria = repositorio.GetCategoria(model.IdCategoria);
                    categoria.Nombre = model.Nombre;
                    categoria.Descripcion = model.Descripcion;
                    repositorio.Update(categoria);
                    repositorio.UnitOfWork.SaveChanges();
                    TempData["success"] = "Categoria editada";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            var categoria = repositorio.GetCategoria(id);
            var model = new NuevaCategoriaModel()
            {
                Nombre = categoria.Nombre,
                IdCategoria = id
            };
            return View(model);

        }
        [HttpPost]
        public ActionResult Delete(NuevaCategoriaModel model)
        {
            try
            {
                var categoria = repositorio.GetCategoria(model.IdCategoria);
                repositorio.Delete(categoria);
                repositorio.UnitOfWork.SaveChanges();
                TempData["success"] = "Cateogira borrada";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
    }
}