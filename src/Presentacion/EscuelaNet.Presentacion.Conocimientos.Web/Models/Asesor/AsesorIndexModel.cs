﻿using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Conocimientos.Web.Models
{
    public class AsesorIndexModel
    {
        public string Titulo { get; set; }
        public int IdCategoria { get; set; }
        public int IdCono { get; set; }
        public List<Asesor> Asesores { get; set; }
    }
}