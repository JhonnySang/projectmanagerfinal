﻿using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Conocimientos.Web.Models
{
    public class NuevoAsesorModel
    {
        public string Titulo { get; set; }
        public int IdCate { get; set; }
        public int IdConocimiento { get; set; }
        public int IdAs { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public Disponibilidad Disponibilidad { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Idioma { get; set; }
        public String Pais { get; set; }
        //public int Id { get; set; }
    }
}