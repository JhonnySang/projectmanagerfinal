﻿using EscuelaNet.Dominio.Proyectos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Proyectos.Web.Models
{
    public class NuevaLineaModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}