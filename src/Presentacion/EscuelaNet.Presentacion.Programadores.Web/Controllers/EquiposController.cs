﻿using EscuelaNet.Dominio.Programadores;
using EscuelaNet.Presentacion.Programadores.Web.Infraestructura;
using EscuelaNet.Presentacion.Programadores.Web.Models;
using EsculaNet.Infraestructura.Programadores.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Programadores.Web.Controllers
{
    public class EquiposController : Controller
    {
        private IEquipoRepository Repositorio = new EquiposRepository();
        // GET: Equipos
        public ActionResult Index()
        {
            var equipos = Repositorio.ListEquipo();

            var model = new EquiposIndexModel()
            {
                Titulo = "Prueba de Equipos",
                Equipos = equipos
            };
            return View(model);
        }
        public ActionResult New()
        {
            var model = new NuevoEquipoModel();
            return View(model);

        }
        [HttpPost]
        public ActionResult New(NuevoEquipoModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var equipo = new Equipo(model.Nombre, model.Pais, model.HusoHorario);
                    Repositorio.Add(equipo);

                    Repositorio.UnitOfWork.SaveChanges();
                    
                    TempData["success"] = "Equipo Creado Correctamente";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }

        }

        public ActionResult Edit(int id)
        {
            if (id <= 0)
            {
                TempData["error"] = "Id del Equipo no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var equipo = Repositorio.GetEquipo(id);
                var model = new NuevoEquipoModel()
                {
                    Nombre = equipo.Nombre,
                    Pais = equipo.Pais,
                    HusoHorario = equipo.HusoHorario,
                    IdEquipo = id
                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(NuevoEquipoModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var equipo = Repositorio.GetEquipo(model.IdEquipo);
                    equipo.Nombre = model.Nombre;
                    equipo.Pais = model.Pais;
                    equipo.HusoHorario = model.HusoHorario;

                    Repositorio.Update(equipo);
                    Repositorio.UnitOfWork.SaveChanges();

                    TempData["success"] = "Equipo Editado Correctamente";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }

        }
        public ActionResult Delete(int id)
        {
            if (id <= 0)
            {
                TempData["error"] = "Id del Equipo no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var equipo = Repositorio.GetEquipo(id);
                var model = new NuevoEquipoModel()
                {
                    Nombre = equipo.Nombre,
                    Pais = equipo.Pais,
                    HusoHorario = equipo.HusoHorario,
                    IdEquipo = id
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Delete(NuevoEquipoModel model)
        {

            try
            {
                var equipo = Repositorio.GetEquipo(model.IdEquipo);
                Repositorio.Delete(equipo);
                Repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Equipo Borrado Correctamente";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }



        }

    }
}