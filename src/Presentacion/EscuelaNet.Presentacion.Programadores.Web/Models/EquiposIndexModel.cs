﻿using EscuelaNet.Dominio.Programadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Programadores.Web.Models
{
    public class EquiposIndexModel
    {
        public string Titulo { get; set; }
        public List<Equipo> Equipos { get; set; }
    }
}